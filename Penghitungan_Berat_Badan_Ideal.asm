org 100h

jmp mulai

pesan1:   db  "Pengitungan Berat Badan Ideal",0dh,0ah,'$'
pesan2:   db  0dh,0ah,"1-Laki laki.",0dh,0ah,"2-Perempuan. $"
pesan3:   db  0dh,0ah,"Masukkan tinggi badan (Cm) : $"
pesan4:   db  0dh,0ah,"Error!! $"
pesan5:   db  0dh,0ah,"Hasilnya : $"
pesan6:   db  0dh,0ah,"Masukkan angka 110 untuk menguranginya : $" 
pesan7:   db  0dh,0ah,"Terimakasih telah menggunakan program. $"

mulai: mov ah,9
       mov dx, offset pesan1
       int 21h 
       mov dx, offset pesan2
       int 21h
       mov ah,0
       int 16h
       cmp al,31h
       je Lakilaki
       cmp al,32h
       je Perempuan
       cmp al,33h
       mov ah,09h
       mov dx, offset pesan4
       int 21h
       mov ah,0
       int 16h
       jmp mulai
       
Lakilaki:  mov ah,09h
           mov dx, offset pesan3
           int 21h
           mov cx,0
           call TambahNo
           push dx
           mov ah,9
           mov dx, offset pesan6
           int 21h 
           mov cx,0
           call TambahNo
           pop bx
           sub bx,dx
           mov dx,bx
           push dx 
           mov ah,9
           mov dx, offset pesan5
           int 21h
           mov cx,10000
           pop dx
           call Lihat
           jmp keluar
           
TambahNo:  mov ah,0
           int 16h 
           mov dx,0  
           mov bx,1 
           cmp al,0dh 
           je FormNo 
           sub ax,30h 
           call LihatNo
           mov ah,0 
           push ax  
           inc cx   
           jmp TambahNo 
           
FormNo:     pop ax  
            push dx      
            mul bx
            pop dx
            add dx,ax
            mov ax,bx       
            mov bx,10
            push dx
            mul bx
            pop dx
            mov bx,ax
            dec cx
            cmp cx,0
            jne FormNo
            ret 
           
Lihat: mov ax,dx
       mov dx,0
       div cx 
       call LihatNo
       mov bx,dx 
       mov dx,0
       mov ax,cx 
       mov cx,10
       div cx
       mov dx,bx 
       mov cx,ax
       cmp ax,0
       jne Lihat
       ret


LihatNo:   push ax 
           push dx 
           mov dx,ax 
           add dl,30h
           mov ah,2
           int 21h
           pop dx  
           pop ax
           ret
      
   
Keluar: mov dx,offset pesan7 
        mov ah, 09h
        int 21h  


        mov ah, 0
        int 16h

        ret 
        
Perempuan: mov ah,09h
           mov dx, offset pesan3
           int 21h
           mov cx,0
           call TambahNo
           push dx
           mov ah,9
           mov dx, offset pesan6
           int 21h 
           mov cx,0
           call TambahNo
           pop bx
           sub bx,dx
           mov dx,bx
           push dx 
           mov ah,9
           mov dx, offset pesan5
           int 21h
           mov cx,10000
           pop dx
           call Lihat
           jmp keluar